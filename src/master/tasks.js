
const moment = require('moment');
const {Key} = require("yarn/lib/cli");

const fs = require('fs').promises;

//Task 1
 function determineCurrentTime(city,offset) {
     let date,utc,newDate;
     const format1 = "YYYY-MM-DD HH:mm:ss a";
      date =new Date();
      utc = date.getTime() +(date.getTimezoneOffset()*60000); // convert  milliseconds to seconds
      newDate = new Date(utc+(3600000*offset)); // convert milliseconds to mins
     return "Current time of "+city+" is "+ moment(newDate).format(format1).toString();

 }
console.log("Task 1 \n")
console.log(determineCurrentTime("Australia",'+10.04'))
console.log(determineCurrentTime('LosAngeles','-07.52'))
console.log(determineCurrentTime('Kolkata','+5.53'))
console.log(determineCurrentTime('Zurich','+00.34'))


// Task 2

 async function readFile() {

    try {
        const data = await fs.readFile('friends.json');
        // console.log (data.toString());
        await readJsonData(data)
    }

    catch (error) {
        console.error(`Got an error trying to read the file: ${error.message}`);
    }

}

async function readJsonData(sampleData) {
    var jsonObject = JSON.parse(sampleData);
    for (let [key, value] of Object.entries(jsonObject)) {
        if (jsonObject[key].length > 3) {
            for (var i = 0; i < value.length; i++) {
                console.log("\n");
                for (let [key1, value1] of Object.entries(value[i])) {
                    console.log("key = " + key1 + ", " + "value = " + value1)

                }
            }
        }

    }

}

console.log("\n Task 2 \n")
readFile()











